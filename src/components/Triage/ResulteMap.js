import React, { createRef, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Container, Avatar, TextField, InputAdornment, Hidden } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import ScrollToTop from '../ScorllToUp';
import { onTotalAct } from '../../Redux/reducer/InfoReducer';




function ResulteMap() {

    const classes = useStyles();
    const data = useSelector((state) => state.info.infomation);
    const Eyes = useSelector((state) => state.info.checkByEyes);
    const Tools = useSelector((state) => state.info.checkByTools);
    const Mouth = useSelector((state) => state.info.checkByMouth);
    const timeStart = useSelector((state) => state.info.startTime);
    const timeEnd = useSelector((state) => state.info.timeEnd);
    const activityTotal = useSelector((state) => state.info.activityTotal);
    const valueOfResulte = useSelector((state) => state.info.valueOfResulte);

    console.log("Resulte", valueOfResulte)

    const dispatch = useDispatch();

    const EyesArray = Object.entries(Eyes);
    const MouthArray = Object.entries({ ...Mouth, E: {}, V: {}, M: {} });
    const [processTime, setprocessTime] = useState(null);
    const _scrollView = createRef(null)

    useEffect(() => {
        const processTime = moment
            .utc(moment(timeEnd).diff(moment(timeStart)))
            .format("mm.ss");
        setprocessTime(processTime);
    }, []);


    const OnShowValue = (value) => {

        if (value === true) {
            return <Typography className={classes.textItemValue}>
                Yes
            </Typography>
        } else if (value === "2") {
            return <Typography className={classes.textItemValue}>
                {'>'}1
                </Typography>
        } else if (value === "1") {
            return <Typography className={classes.textItemValue}>
                1
            </Typography>
        } else if (value === parseInt(value) && value !== 0) {
            return <Typography className={classes.textItemValue}>
                {value}
            </Typography>
        } else if (value === parseInt(0)) {
            return <Typography className={classes.textItemValue}>
                N/A
            </Typography>
        }
        else {
            return <Typography className={classes.textItemValue}>
                N/A
                </Typography>
        }
    }

    const onTotal = () => {
        const Lab = parseInt(Mouth.lab.value);
        const Operative = parseInt(Mouth.operative.value);
        const injection = parseInt(Mouth.injection.value)

        const total = Lab + Operative + injection

        dispatch(onTotalAct(total));
    }

    useEffect(() => {
        onTotal()
    })


    return (
        <Grid ref={_scrollView} className={classes.gridBody}>
            <Grid container>
                <Container>
                    <Paper className={classes.paperStyle}>
                        <Container className={classes.containerBodyStyle}>
                            <Grid spacing={2} container direction="row" className={classes.gridSummary}>

                                <Grid item xl={8} xs={12} container alignItems="center" direction="row">
                                    <Grid className={classes.gridAvatarInSum}>
                                        <Avatar className={classes.avaterSum}>R</Avatar>
                                    </Grid>

                                    <Grid>
                                        <Grid>
                                            <Typography className={classes.summaryText}>
                                                Summary
                                            </Typography>
                                        </Grid>
                                        <Grid>
                                            <Typography className={classes.resuscitationText}>
                                                Resuscitation
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid item xl={4} xs={12} container alignItems="center" direction="row" >
                                    <Grid container alignItems="center" justify="flex-end" direction="row">
                                        <Grid container item xl={5} xs={12} direction="row" >
                                            <Grid item xl={4} xs={5}>
                                                <Typography className={classes.textHeaderTime}>
                                                    เริ่มต้น :
                                            </Typography>
                                            </Grid>

                                            <Grid>
                                                <Typography className={classes.textTime}>
                                                    {moment(timeStart).format('h:mm:ss a')}
                                                </Typography>
                                            </Grid>

                                        </Grid>

                                        <Grid container item xl={5} xs={12} direction="row" >
                                            <Grid item xl={4} xs={5}>
                                                <Typography className={classes.textHeaderTime}>
                                                    สิ้นสุด :
                                            </Typography>
                                            </Grid>

                                            <Grid>
                                                <Typography className={classes.textTime}>
                                                    {moment(timeEnd).format('h:mm:ss a')}
                                                </Typography>
                                            </Grid>

                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" justify="flex-end" direction="row">
                                        <Grid container item xl={8} xs={12} direction="row" alignItems="center" >
                                            <Grid item xl={5} xs={5}>
                                                <Typography className={classes.textHeaderTime}>
                                                    เวลาที่ประเมิณ :
                                            </Typography>
                                            </Grid>

                                            <Grid item xl={6} xs={5}>
                                                <Typography className={classes.textTime}>
                                                    {`${processTime} นาที`}
                                                </Typography>
                                            </Grid>

                                        </Grid>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </Container>

                        <Grid container alignItems="center" className={classes.gridBodyInfoName}>
                            <Container className={classes.containerBodyInfoName}>
                                <Grid alignItems="center" container direction="row">
                                    <Grid item xl={10} xs={7} container direction="row" >
                                        <Grid className={classes.gridInfoName}>
                                            <Typography className={classes.textInfoName}>
                                                ชื่อ
                                            </Typography>
                                        </Grid>

                                        <Grid>
                                            <Typography className={classes.textInfoName}>
                                                {data.patientInfo.value === "" ?
                                                    "-"
                                                    :
                                                    data.patientInfo.value
                                                }
                                            </Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid item xl={2} xs={5} container direction="row" alignItems="center" justify="flex-end">
                                        <Grid className={classes.gridInfoName}>
                                            <Typography className={classes.textInfoName}>
                                                อายุ
                                            </Typography>
                                        </Grid>

                                        <Grid >
                                            <Typography className={classes.textInfoName}>
                                                {data.age.value === "" ?
                                                    "- "
                                                    :
                                                    data.age.value
                                                }

                                                {" " + data.ageUnit.value}
                                            </Typography>
                                        </Grid>
                                    </Grid>

                                </Grid>



                            </Container>
                        </Grid>

                        <Container className={classes.containerTools}>
                            <Grid container justify="center" alignItems="center" direction="row">

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={8} xl={6}>
                                        <Typography className={classes.textHeaderTools}>
                                            Temp :
                                        </Typography>
                                    </Grid>

                                    <Grid >
                                        <Typography className={classes.textValueTools}>
                                            {Tools.temp.value === "" ?
                                                "N/A"
                                                :
                                                Tools.temp.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={4} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={3} xl={4}>
                                        <Typography className={classes.textHeaderTools}>
                                            BP :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography className={classes.textValueTools}>
                                            {
                                                Tools.BP1.value === "" && Tools.BP2.value === "" ?
                                                    "N/A"
                                                    :
                                                    Tools.BP1.value + " / " + Tools.BP2.value
                                            }

                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={2} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={6} xl={5}>
                                        <Typography className={classes.textHeaderTools}>
                                            PR :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography className={classes.textValueTools}>
                                            {Tools.PR.value === "" ?
                                                "N/A"
                                                :
                                                Tools.PR.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={5} xl={5}>
                                        <Typography className={classes.textHeaderTools}>
                                            RR :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography className={classes.textValueTools}>
                                            {Tools.RR.value === "" ?
                                                "N/A"
                                                :
                                                Tools.RR.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </Container>

                        <Hidden xsDown >
                            <Container className={classes.containerBodyXs}>
                                {
                                    EyesArray.map((item, index) =>
                                        <Grid container direction="row" justify="center" alignItems="center" className={classes.gridBodyItemXs}>
                                            <Grid container direction="row" alignItems="center" className={classes.gridInBodyItem}>
                                                <Grid container item xs={11} xl={11} className={classes.gridHeaderTextItem}>
                                                    <Typography className={classes.textItem}>
                                                        {item[1].title}
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={1} xl={1} container justify="flex-end">
                                                    <Typography className={classes.textItemValue}>
                                                        {
                                                            item[1].value === "" ?
                                                                "N/A"
                                                                :
                                                                "Yes"
                                                        }
                                                    </Typography>
                                                </Grid>

                                            </Grid>
                                        </Grid>
                                    )
                                }

                                {
                                    MouthArray.map((item, index) =>
                                        <Grid container direction="row" justify="center" alignItems="center" className={classes.gridBodyItemXs}>
                                            {item[0] !== "E" && item[0] !== "M" && item[0] !== "V" ?
                                                <Grid container direction="row" alignItems="center" className={classes.gridInBodyItem}>
                                                    <Grid container item xs={11} className={classes.gridHeaderTextItem}>
                                                        <Typography className={classes.textItem}>
                                                            {item[1].title}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={1} container justify="flex-end">
                                                        {OnShowValue(item[1].value)}
                                                    </Grid>
                                                </Grid>
                                                :
                                                <Grid> </Grid>

                                            }

                                        </Grid>
                                    )
                                }

                            </Container>
                        </Hidden>

                        <Hidden smUp >
                            <Container className={classes.containerBodySm}>


                                {
                                    EyesArray.map((item, index) =>
                                        <Grid container direction="row" justify="center" alignItems="center" >
                                            <Grid container direction="row" alignItems="center" className={classes.gridInBodyItem}>
                                                <Grid container item xs={11} xl={11} className={classes.gridHeaderTextItem}>
                                                    <Typography className={classes.textItem}>
                                                        {item[1].title}
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={1} xl={1} container justify="flex-end">
                                                    <Typography className={classes.textItemValue}>
                                                        {
                                                            item[1].value === "" ?
                                                                "N/A"
                                                                :
                                                                "Yes"
                                                        }
                                                    </Typography>
                                                </Grid>

                                            </Grid>
                                        </Grid>
                                    )
                                }

                                {
                                    MouthArray.map((item, index) =>
                                        <Grid container direction="row" justify="center" alignItems="center" >
                                            {item[0] !== "E" && item[0] !== "M" && item[0] !== "V" ?
                                                <Grid container direction="row" alignItems="center" className={classes.gridInBodyItem}>
                                                    <Grid container item xs={11} className={classes.gridHeaderTextItem}>
                                                        <Typography className={classes.textItem}>
                                                            {item[1].title}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={1} container justify="flex-end">
                                                        {OnShowValue(item[1].value)}
                                                    </Grid>
                                                </Grid>
                                                :
                                                <Grid> </Grid>

                                            }

                                        </Grid>
                                    )
                                }

                            </Container>
                        </Hidden>

                        <Hidden smUp >
                            <Container className={classes.containerTotalSm}>
                                <Grid item xl={12} container alignItems="center" justify="flex-end" direction="row" className={classes.gridBodyTotal}>
                                    <Grid item xl={4} sm={5} md={5} xs={8}>
                                        <Typography className={classes.textAllTotal}>
                                            จำนวนกิจกรรมทั้งหมด
                                        </Typography>
                                    </Grid>

                                    <Grid xs={4} xl={2} md={2}>
                                        <TextField size="small" disabled variant="outlined" className={classes.textFieldTotal}
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <Typography className={classes.textInTextFieldTotal}>
                                                            {activityTotal}
                                                        </Typography>
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </Container>
                        </Hidden>

                        <Hidden xsDown >
                            <Container className={classes.containerTotalXs}>
                                <Grid item xl={12} container alignItems="center" justify="flex-end" direction="row" className={classes.gridBodyTotal}>
                                    <Grid item xl={4} sm={5} md={5} xs={8}>
                                        <Typography className={classes.textAllTotal}>
                                            จำนวนกิจกรรมทั้งหมด
                                        </Typography>
                                    </Grid>

                                    <Grid xs={4} xl={2} md={2}>
                                        <TextField size="small" disabled variant="outlined" className={classes.textFieldTotal}
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <Typography className={classes.textInTextFieldTotal}>
                                                            {activityTotal}
                                                        </Typography>
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </Container>
                        </Hidden>
                    </Paper>
                    <ScrollToTop />
                </Container>
            </Grid>
        </Grid >

    );
}

export default ResulteMap;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    absolute: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: "max(min(6vw),6vw)",
        zIndex: 99,
        left: "min(max(83vw),85vw)",
    },
    gridBody: {
        backgroundColor: "#FAFAFA",
        width: "100%",
        marginBottom: 30
    },
    paperStyle: {
        paddingBottom: 47
    },
    containerBodyStyle: {
        paddingTop: 23
    },
    gridSummary: {
        marginBottom: 22,
        paddingLeft: "2vw",
        paddingRight: "2vw"
    },
    gridAvatarInSum: {
        marginRight: 21
    },
    avaterSum: {
        width: 64,
        height: 64,
        backgroundColor: "#F35050",
        fontSize: 36,
        fontFamily: "Kanit, sans-serif"
    },
    summaryText: {
        color: "#3C68A2",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif"
    },
    resuscitationText: {
        color: "#5B81DC",
        fontSize: 24,
        fontFamily: "Kanit, sans-serif"
    },
    textHeaderTime: {
        color: "#3C68A2",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    },
    textTime: {
        color: "#819DE0",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    },
    gridBodyInfoName: {
        backgroundColor: "#5B81DC",
        height: 46
    },
    containerBodyInfoName: {
        marginLeft: "4vw",
        marginRight: "4vw"
    },
    gridInfoName: {
        marginRight: 30
    },
    textInfoName: {
        color: "#FFFFFF",
        fontWeight: "bold",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif"
    },
    containerTools: {
        marginTop: 8
    },
    textHeaderTools: {
        color: "#3C68A2",
        fontWeight: "bold",
        fontSize: "min(max(16px , 12px),14px)",
        fontFamily: "Kanit, sans-serif"
    },
    textValueTools: {
        color: "#819DE0",
        fontSize: "min(max(16px , 12px),14px)",
        fontFamily: "Kanit, sans-serif"
    },
    containerBodyXs: {
        marginTop: 30,
        paddingLeft: "10vw",
        paddingRight: "10vw"
    },
    containerBodySm: {
        marginTop: 30,
        paddingLeft: "9w",
        paddingRight: "9vw"
    },
    gridBodyItemXs: {
        paddingLeft: "5vw",
        paddingRight: "5vw"
    },
    gridInBodyItem: {
        borderBottom: "solid",
        borderBottomColor: "#5B81DC"
    },
    gridHeaderTextItem: {
        paddingBottom: 5,
        paddingTop: 5
    },
    textItem: {
        color: "#5B81DC",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    },
    textItemValue: {
        color: "#3C68A2",
        fontWeight: "bold",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    },
    containerTotalSm: {
        paddingLeft: "9vw",
        paddingRight: "9vw"
    },
    containerTotalXs: {
        paddingLeft: "15vw",
        paddingRight: "15vw"
    },

    gridBodyTotal: {
        marginTop: 30.5
    },
    textAllTotal: {
        color: "#819DE0",
        fontSize: 18,
        fontFamily: "Kanit, sans-serif"
    },
    textFieldTotal: {
        backgroundColor: "#819DE05C"
    },
    textInTextFieldTotal: {
        color: "#FFFFFF",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    }
}));
