import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Container, TextField } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { checkByToolsAction } from '../../Redux/reducer/InfoReducer';




function CheckByTools() {

    const classes = useStyles();
    const data = useSelector((state) => state.info.checkByTools);
    const dispatch = useDispatch();



    const OnInput = (e, key) => {
        if (e >= 999) {
            const value = 999
            dispatch(checkByToolsAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e < 0) {
            const value = ""
            dispatch(checkByToolsAction(({ ...data, [key]: { ...data[key], value } })));
        }
        else {
            const value = e
            dispatch(checkByToolsAction(({ ...data, [key]: { ...data[key], value } })));
        }

    };

    return (
        <Grid className={classes.gridBody}>
            <Grid container >
                <Container >
                    <Paper className={classes.paperStyle}>
                        <Grid className={classes.gridPaperBody}>
                            <Grid className={classes.gridHeaderText}>
                                <Typography className={classes.headerText}>
                                    Check symbol by tools
                                </Typography>
                            </Grid>

                            <Container className={classes.containerStyle}>
                                <Grid
                                    container
                                    direction="column"
                                    justify="center"
                                    alignItems="center"

                                >
                                    <Grid container direction="row" justify="center" alignItems="center" className={classes.gridItemBody}>
                                        <Grid item xs={2} xl={1}>
                                            <Typography className={classes.textItem}>
                                                Temp
                                        </Typography>
                                        </Grid>

                                        <Grid >
                                            <TextField
                                                onChange={(v) => {
                                                    OnInput(v.target.value, "temp")
                                                }}
                                                inputProps={{ max: 999 }}
                                                type="number"
                                                placeholder="0.0 C"
                                                value={data.temp.value}
                                                size="small"
                                                variant="outlined"
                                                className={classes.textFieldStyle} />
                                        </Grid>

                                    </Grid>

                                    <Grid container direction="row" justify="center" alignItems="center" className={classes.gridItemBody}>
                                        <Grid item xs={2} xl={1}>
                                            <Typography className={classes.textItem}>
                                                RR
                                        </Typography>
                                        </Grid>

                                        <Grid>
                                            <TextField

                                                onChange={(v) => {
                                                    OnInput(v.target.value, "RR")
                                                }}
                                                inputProps={{ max: 999 }}
                                                type="number"
                                                placeholder="0"
                                                value={data.RR.value}
                                                size="small"
                                                variant="outlined"
                                                className={classes.textFieldStyle}
                                            />
                                        </Grid>

                                    </Grid>

                                    <Grid container direction="row" justify="center" alignItems="center" className={classes.gridItemBody}>
                                        <Grid item xs={2} xl={1}>
                                            <Typography className={classes.textItem}>
                                                PR
                                        </Typography>
                                        </Grid>

                                        <Grid >
                                            <TextField
                                                onChange={(v) => {
                                                    OnInput(v.target.value, "PR")
                                                }}
                                                inputProps={{ max: 999 }}
                                                type="number"
                                                placeholder="0"
                                                value={data.PR.value}
                                                size="small"
                                                variant="outlined"
                                                className={classes.textFieldStyle}
                                            />
                                        </Grid>

                                    </Grid>

                                    <Grid container direction="row" justify="center" alignItems="center" className={classes.gridItemBody}>
                                        <Grid item xs={2} xl={1}>
                                            <Typography className={classes.textItem}>
                                                BP
                                        </Typography>
                                        </Grid>

                                        <Grid id="InputBP">
                                            <TextField
                                                onChange={(v) => {
                                                    OnInput(v.target.value, "BP1")
                                                }}
                                                inputProps={{ max: 999 }}
                                                type="number"
                                                placeholder="0"
                                                value={data.BP1.value}
                                                size="small"
                                                variant="outlined"
                                                className={classes.textBPStyle} />
                                        </Grid>

                                        <Grid className={classes.gridTextInBP}>
                                            <Typography className={classes.textInBPStyle}>
                                                /
                                        </Typography>
                                        </Grid>

                                        <Grid id="InputBP">
                                            <TextField
                                                onChange={(v) => {

                                                    OnInput(v.target.value, "BP2")
                                                }}
                                                inputProps={{ max: 999 }}
                                                type="number"
                                                placeholder="0"
                                                value={data.BP2.value}
                                                size="small"
                                                variant="outlined"
                                                className={classes.textBPStyle} />
                                        </Grid>

                                    </Grid>

                                </Grid>
                            </Container>

                        </Grid>
                    </Paper>
                </Container>

            </Grid>

        </Grid>

    );
}

export default CheckByTools;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    gridBody: {
        backgroundColor: "#FAFAFA",
        width: "100%"
    },
    paperStyle: {
        paddingBottom: 83
    },
    gridPaperBody: {
        paddingTop: 37,
        textAlign: "center"
    },
    gridHeaderText: {
        marginBottom: 57
    },
    headerText: {
        color: "#5B81DC",
        fontSize: 20,
        fontWeight: "bold",
        fontFamily: "Kanit, sans-serif"
    },
    containerStyle: {
        paddingLeft: "5vw",
        paddingRight: "5vw"
    },
    gridItemBody: {
        marginBottom: 16
    },
    textItem: {
        color: "#5B81DC",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif",
        marginRight: 18
    },
    textFieldStyle: {
        width: 131
    },
    textBPStyle: {
        width: 52
    },
    gridTextInBP: {
        marginLeft: 8, marginRight: 8
    },
    textInBPStyle: {
        color: "#5B81DC",
        fontSize: 24,
        fontFamily: "Kanit, sans-serif"
    }
}));
