import React, { useState, useEffect, createRef } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { Typography, Paper, Container, TextField, Switch, Checkbox, IconButton, } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { checkByMouthAction } from '../../Redux/reducer/InfoReducer';

import ScrollToTop from '../ScorllToUp';





function CheckByMouth() {

    const classes = useStyles();

    const [checked, setChecked] = React.useState([]);
    const data = useSelector((state) => state.info.checkByMouth);
    const dispatch = useDispatch();

    const [GSCE, setGSCE] = useState(0);
    const [GSCV, setGSCV] = useState(0);
    const [GSCM, setGSCM] = useState(0);

    const [painScore, setPainScore] = useState(1);
    const [Spo2, setSpo2] = useState(0);

    const _scrollView = createRef(null)





    const setStringToNumber = (e, key) => {

        if (e >= 4 && key === "E") {
            const value = parseInt(4)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e >= 5 && key === "V") {
            const value = parseInt(5)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e >= 6 && key === "M") {
            const value = parseInt(6)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e >= 10 && key === "Pain") {
            const value = parseInt(10)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e >= 100 && key === "SpO2") {
            const value = parseInt(100)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }
        else {
            const value = parseInt(e)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const handleSwitch = (event, status) => () => {
        const currentIndex = checked.indexOf(event);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            const value = !status
            newChecked.push(event);
            dispatch(checkByMouthAction(({ ...data, [event]: { ...data[event], value } })));
        } else {
            const value = !status
            newChecked.splice(currentIndex, 1);
            dispatch(checkByMouthAction(({ ...data, [event]: { ...data[event], value } })));
        }

        setChecked(newChecked);
    }


    const testHandleCheck = (event, value) => () => {
        if (value === "1") {
            dispatch(checkByMouthAction(({ ...data, [event]: { ...data[event], value } })));
        } else if (value === "2") {
            dispatch(checkByMouthAction(({ ...data, [event]: { ...data[event], value } })));
        } else if (value === "0") {
            const value = 0
            dispatch(checkByMouthAction(({ ...data, [event]: { ...data[event], value } })));
        }
    }

    const onAddGSCE = (e) => {
        if (e === "+" && data.E.value < 4) {
            const value = data.E.value + 1;
            const key = "E"
            setGSCE(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e === "-" && data.E.value > 0) {
            const value = data.E.value - 1;
            const key = "E"
            setGSCE(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const onAddGSCV = (e) => {
        if (e === "+" && data.V.value < 5) {
            const value = data.V.value + 1;
            const key = "V"
            setGSCV(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e === "-" && data.V.value > 0) {
            const value = data.V.value - 1;
            const key = "V"
            setGSCV(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const onAddGSCM = (e) => {

        if (e === "+" && data.M.value < 6) {
            const value = data.M.value + 1;
            const key = "M"
            setGSCM(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e === "-" && data.M.value > 0) {
            const value = data.M.value - 1;
            const key = "M"
            setGSCM(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const onAddPainScore = (e) => {
        if (e === "+" && data.Pain.value < 10) {
            const value = data.Pain.value + 1;
            const key = "Pain"
            setPainScore(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e === "-" && data.Pain.value > 1) {
            const value = data.Pain.value - 1;
            const key = "Pain"
            setPainScore(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const onAddSpo2 = (e) => {
        if (e === "+" && data.SpO2.value < 100) {
            const value = data.SpO2.value + 1;
            const key = "SpO2"
            setSpo2(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        } else if (e === "-" && data.SpO2.value > 0) {
            const value = data.SpO2.value - 1;
            const key = "SpO2"
            setSpo2(value)
            dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));
        }

    }

    const calGSCScore = () => {
        const E = data.E.value;
        const V = data.V.value;
        const M = data.M.value
        const key = "TotalGSC";
        const value = E + V + M
        dispatch(checkByMouthAction(({ ...data, [key]: { ...data[key], value } })));


    }


    useEffect(() => {
        calGSCScore()
    }, [data.E.value, data.V.value, data.M.value])

    return (
        <Grid ref={_scrollView} className={classes.gridBody}>
            <Grid container >
                <Container>
                    <Paper className={classes.paperStyle}>
                        <Grid className={classes.gridPaperBody}>
                            <Grid className={classes.gridHeaderText}>
                                <Typography className={classes.headerText}>
                                    Check symbol by mouth
                                </Typography>
                            </Grid>

                            <Container className={classes.containerInPaper}>
                                <Grid
                                    container
                                    direction="column"
                                >
                                    <Grid>
                                        <Typography className={classes.headerTextItem}>
                                            กิจกรรม
                                            </Typography>
                                        <div className={classes.lineHeaderTextActivity} />

                                    </Grid>

                                    <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                        justify="flex-start"

                                    >
                                        <Grid item xl={2} xs={3} >
                                            <Typography className={classes.textItemInBody}>

                                            </Typography>
                                        </Grid>


                                        <Grid item xs={2} xl={1} className={classes.gridNumberOne}>
                                            <Typography className={classes.textItemInBody}>
                                                1
                                            </Typography>
                                        </Grid>

                                        <Grid className={classes.gridNumberThanOne} >
                                            <Typography className={classes.textItemInBody}>
                                                {'>'}1
                                            </Typography>
                                        </Grid>

                                    </Grid>

                                    <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                        justify="flex-start"
                                    >
                                        <Grid item xl={2} xs={3} >
                                            <Typography className={classes.textItemInBody}>
                                                LAB
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={2} xl={1}>
                                            <Checkbox
                                                checked={data.lab.value === "1"}
                                                onChange={testHandleCheck('lab', data.lab.value == 1 ? "0" : "1")}
                                                color="default"
                                            />
                                        </Grid>

                                        <Grid >
                                            <Checkbox
                                                checked={data.lab.value === "2"}
                                                onChange={testHandleCheck('lab', data.lab.value == 2 ? "0" : "2")}
                                            />
                                        </Grid>

                                    </Grid>

                                    <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                    >
                                        <Grid item xl={2} xs={3}>
                                            <Typography className={classes.textItemInBody}>
                                                หัตถการ
                                            </Typography>
                                        </Grid>


                                        <Grid item xs={2} xl={1}>
                                            <Checkbox
                                                color="default"
                                                checked={data.operative.value === "1"}
                                                onChange={testHandleCheck('operative', data.operative.value == 1 ? "0" : "1")}
                                            />
                                        </Grid>

                                        <Grid >
                                            <Checkbox
                                                checked={data.operative.value === "2"}
                                                onChange={testHandleCheck('operative', data.operative.value == 2 ? "0" : "2")}
                                                color="default"
                                            />
                                        </Grid>

                                    </Grid>


                                    <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                    >
                                        <Grid item xl={2} xs={3} >
                                            <Typography className={classes.textItemInBody}>
                                                Injection
                                            </Typography>
                                        </Grid>


                                        <Grid item xs={2} xl={1}>
                                            <Checkbox
                                                color="default"
                                                checked={data.injection.value === "1"}
                                                onChange={testHandleCheck('injection', data.injection.value == 1 ? "0" : "1")}
                                            />
                                        </Grid>

                                        <Grid >
                                            <Checkbox
                                                color="default"
                                                checked={data.injection.value === "2"}
                                                onChange={testHandleCheck('injection', data.injection.value == 2 ? "0" : "2")}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid className={classes.gridBodyItem}>
                                    <Typography className={classes.headerTextItem}>
                                        ความจำเป็นต่อการใช้อุปกรณ์
                                            </Typography>
                                    <div className={classes.lineHeaderTextTools} />

                                    <Grid
                                        container
                                        direction="row"
                                        alignItems="center"
                                        className={classes.gridInBodyTools}
                                    >
                                        <Grid item xs={6}>
                                            <Grid container alignItems="center" direction="row">
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        X-ray
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.xRay.value === true}
                                                        color="default"
                                                        onChange={handleSwitch('xRay', data.xRay.value)}
                                                    />
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" direction="row" >
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        Suture
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.suture.value === true}
                                                        onChange={handleSwitch('suture', data.suture.value)}
                                                        color="default"
                                                    />
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" direction="row">
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        EKG
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.EKG.value === true}
                                                        onChange={handleSwitch('EKG', data.EKG.value)}
                                                        color="default"
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>

                                        <Grid item xs={6} >
                                            <Grid container alignItems="center" direction="row" >
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        Consult
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.consult.value === true}
                                                        onChange={handleSwitch('consult', data.consult.value)}
                                                        color="default"
                                                    />
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" direction="row" >
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        IV Fluid
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.IVfluid.value === true}
                                                        onChange={handleSwitch('IVfluid', data.IVfluid.value)}
                                                        color="default"
                                                    />
                                                </Grid>
                                            </Grid>

                                            <Grid container alignItems="center" direction="row">
                                                <Grid item xs={6} xl={4}>
                                                    <Typography className={classes.textItemInBody}>
                                                        Ultrasound
                                                    </Typography>
                                                </Grid>

                                                <Grid item xs={2}>
                                                    <Checkbox
                                                        checked={data.ultrasound.value === true}
                                                        onChange={handleSwitch('ultrasound', data.ultrasound.value)}
                                                        color="default"
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid className={classes.gridBodyItem}>
                                    <Typography className={classes.headerTextItem}>
                                        ความเสี่ยง
                                            </Typography>
                                    <div className={classes.lineHeaderTextRisk} />

                                    <Grid container alignItems="center" direction="row" className={classes.gridInBodyTools}>
                                        <Grid item xl={4} xs={7}>
                                            <Typography className={classes.textItemInBody}>
                                                Suicidal attemps
                                            </Typography>
                                        </Grid>

                                        <Grid>
                                            <Switch
                                                value={data.suicidal.value}
                                                onChange={handleSwitch('suicidal', data.suicidal.value)}
                                                checked={data.suicidal.value !== false}
                                            />
                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" direction="row">
                                        <Grid item xl={4} xs={7}>
                                            <Typography className={classes.textItemInBody}>
                                                High mechanism of injury + ความเสี่ยงอื่นๆ
                                            </Typography>
                                        </Grid>

                                        <Grid>
                                            <Switch
                                                value={data.highMechanism.value}
                                                onChange={handleSwitch('highMechanism', data.highMechanism.value)}
                                                checked={data.highMechanism.value !== false}
                                            />
                                        </Grid>

                                    </Grid>
                                </Grid>

                                <Grid className={classes.gridBodyItem}>
                                    <Typography className={classes.headerTextItem}>
                                        คะแนนประเมิณ
                                    </Typography>
                                    <div className={classes.lineHeaderTextAverage} />

                                    <Grid className={classes.gridTextGSC}>
                                        <Typography className={classes.textGSC}>
                                            GSC Score
                                    </Typography>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" className={classes.gridInBodyEVM}>
                                        <Grid id="GSC" item xs={1} xl={1}>
                                            <Typography className={classes.textItemInBody}>
                                                E
                                            </Typography>
                                        </Grid>

                                        <Grid >
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                size="small"
                                                type="number"
                                                defaultValue={GSCE}
                                                value={data.E.value}
                                                className={classes.textFieldEVM}
                                                inputProps={{ max: 4, }}
                                                onChange={(e) => {
                                                    setStringToNumber(e.target.value = Math.max(0, e.target.value), "E");
                                                }} />
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCE("+")}>
                                                    <AddCircleIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>
                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCE("-")} >
                                                    <RemoveCircleOutlineIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" direction="row">
                                        <Grid id="GSC" item xs={1} xl={1}>
                                            <Typography className={classes.textItemInBody}>
                                                V
                                            </Typography>
                                        </Grid>

                                        <Grid >
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                size="small"
                                                type="number"
                                                defaultValue={GSCV}
                                                value={data.V.value}
                                                className={classes.textFieldEVM}
                                                inputProps={{ max: 5, }}
                                                onChange={(e) => {
                                                    setStringToNumber(e.target.value = Math.max(0, e.target.value), "V");
                                                }} />
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCV("+")}>
                                                    <AddCircleIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>
                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCV("-")} >
                                                    <RemoveCircleOutlineIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" direction="row">
                                        <Grid id="GSC" item xs={1} xl={1} >
                                            <Typography className={classes.textItemInBody}>
                                                M
                                            </Typography>
                                        </Grid>

                                        <Grid >
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                size="small"
                                                type="number"
                                                defaultValue={GSCM}
                                                value={data.M.value}
                                                className={classes.textFieldEVM}
                                                inputProps={{ max: 6, }}
                                                onChange={(e) => {
                                                    setStringToNumber(e.target.value = Math.max(0, e.target.value), "M");
                                                }}
                                            />
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCM("+")} >
                                                    <AddCircleIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>
                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddGSCM("-")}>
                                                    <RemoveCircleOutlineIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" direction="row" className={classes.gridTextFieldGSCTotal}>
                                        <Grid className={classes.gridInGSCTtoal}>
                                            <Typography className={classes.textGSCTotal}>
                                                GSC Total Score
                                            </Typography>
                                        </Grid>

                                        <Grid className={classes.gridInBodyTextFieldGSCTotal}>
                                            <TextField
                                                className={classes.textFieldGSCTotal}
                                                size="small"
                                                value={data.TotalGSC.value}
                                                disabled
                                                defaultValue={data.TotalGSC.value}
                                                variant="outlined"
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row">
                                        <Grid item xl={2} xs={5}>
                                            <Typography className={classes.textItemInBody}>
                                                Pain scorer(1-10)
                                            </Typography>
                                        </Grid>

                                        <Grid item xl={2} xs={3} >
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                size="small"
                                                defaultValue={painScore}
                                                type="number"
                                                value={data.Pain.value}
                                                className={classes.textfieldPainAndO2}
                                                inputProps={{ max: 10, }}
                                                onChange={(e) => {
                                                    setStringToNumber(e.target.value = Math.max(0, e.target.value), "Pain");
                                                }}
                                            />
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddPainScore("+")}>
                                                    <AddCircleIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton aria-label="upload picture" component="span" onClick={() => onAddPainScore("-")}>
                                                    <RemoveCircleOutlineIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                    </Grid>

                                    <Grid container alignItems="center" direction="row">
                                        <Grid item xl={2} xs={5}>
                                            <Typography className={classes.textItemInBody}>
                                                SpO2(0-100%)
                                            </Typography>
                                        </Grid>

                                        <Grid item xl={2} xs={3}>
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                size="small"
                                                defaultValue={Spo2}
                                                value={data.SpO2.value}
                                                type="number"
                                                className={classes.textfieldPainAndO2}
                                                inputProps={{ max: 100, }}
                                                onChange={(e) => {
                                                    setStringToNumber(e.target.value = Math.max(0, e.target.value), "SpO2");
                                                }}
                                            />
                                        </Grid>

                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton component="span" onClick={() => onAddSpo2("+")} >
                                                    <AddCircleIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>
                                        <Grid>
                                            <label htmlFor="icon-button-file">
                                                <IconButton component="span" onClick={() => onAddSpo2("-")} >
                                                    <RemoveCircleOutlineIcon className={classes.iconStyle} />
                                                </IconButton>
                                            </label>
                                        </Grid>

                                    </Grid>
                                </Grid>
                            </Container>
                        </Grid>
                    </Paper>
                </Container>


                <ScrollToTop />

            </Grid>




        </Grid>

    );
}

export default CheckByMouth;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    absolute: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: "max(min(6vw),6vw)",
        left: "min(max(83vw),85vw)",
        zIndex: 99
    },
    gridBody: {
        backgroundColor: "#FAFAFA",
        width: "100%"
    },
    paperStyle: {
        paddingBottom: 83
    },
    gridPaperBody: {
        paddingTop: 37,
    },
    gridHeaderText: {
        marginBottom: 57,
        textAlign: "center"
    },
    headerText: {
        color: "#5B81DC",
        fontSize: 20,
        fontWeight: "bold",
        fontFamily: "Kanit, sans-serif"
    },
    containerInPaper: {
        paddingLeft: "5vw",
        paddingRight: "5vw"
    },
    headerTextItem: {
        color: "#FFBB00",
        fontSize: 18,
        fontStyle: "italic",
        fontFamily: "Kanit, sans-serif",
        textAlign: "start"
    },
    gridNumberOne: {
        paddingLeft: 15
    },
    gridNumberThanOne: {
        paddingLeft: 13
    },
    lineHeaderTextActivity: {
        height: 2,
        width: 36,
        backgroundColor: "#FFBB00"
    },
    lineHeaderTextTools: {
        height: 2,
        width: 147,
        backgroundColor: "#FFBB00"
    },
    lineHeaderTextAverage: {
        height: 2,
        width: 46.87,
        backgroundColor: "#FFBB00"
    },
    lineHeaderTextRisk : {
        height: 2, 
        width: 46, 
        backgroundColor: "#FFBB00" 
    },
    textItemInBody: {
        color: "#5B81DC",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif"
    },
    gridBodyItem: {
        marginTop: 32
    },

    gridInBodyTools: {
        marginTop: 11
    },
    gridTextGSC: {
        marginTop: 20
    },
    textGSC: {
        color: "#F6D169",
        fontSize: 18,
        fontFamily: "Kanit, sans-serif",
        textAlign: "start"
    },
    gridInBodyEVM: {
        marginTop: 16
    },
    textFieldEVM: {
        width: 150
    },
    iconStyle: {
        color: "#FBD57D",
        fontSize: 24
    },
    textGSCTotal: {
        color: "#5B81DC",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif",
        padding: 9
    },
    gridTextFieldGSCTotal: {
        marginTop: 22,
        marginBottom: 20
    },
    gridInBodyTextFieldGSCTotal: {
        marginLeft: 16
    },
    textFieldGSCTotal: {
        width: 150,
        color: "#C8C8C8",
        backgroundColor: "#F9F2DE",
        borderColor: "#FFFFFF"
    },
    textfieldPainAndO2: {
        width: "100%",
    },
    gridInGSCTtoal : {
        backgroundColor: "#FFBB00"
    }
}));




// bottom: -490,
// right: 206,