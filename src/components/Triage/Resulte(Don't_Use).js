import React, { createRef, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Container, TextField, Avatar, InputAdornment, Fab } from '@material-ui/core';
import { useSelector } from 'react-redux';
import moment from 'moment';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';




function Resulte() {

    const classes = useStyles();
    const data = useSelector((state) => state.info.infomation);
    const Eyes = useSelector((state) => state.info.checkByEyes);
    const Tools = useSelector((state) => state.info.checkByTools);
    const Mouth = useSelector((state) => state.info.checkByMouth);
    const timeStart = useSelector((state) => state.info.startTime);
    const timeEnd = useSelector((state) => state.info.timeEnd);

    const [processTime, setprocessTime] = useState(null);
    const _scrollView = createRef(null)

    useEffect(() => {
        const processTime = moment
            .utc(moment(timeEnd).diff(moment(timeStart)))
            .format("mm.ss");
        setprocessTime(processTime);
    }, []);

    const _scrollToTop = () => {
        if (!!_scrollView.current) {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }
    };

    return (
        <Grid ref={_scrollView} style={{ backgroundColor: "#FAFAFA", width: "100%", marginBottom: 30 }}>
            <Grid container style={{}}>
                <Container style={{}}>
                    <Paper style={{ paddingBottom: 47 }}>
                        <Container style={{ paddingTop: 23 }}>
                            <Grid container direction="row" style={{ marginBottom: 22, paddingLeft: "2vw", paddingRight: "2vw" }}>

                                <Grid item xl={8} xs={5} container alignItems="center" direction="row" style={{}}>
                                    <Grid style={{ marginRight: 21 }}>
                                        <Avatar style={{ width: 64, height: 64, backgroundColor: "#F35050", fontSize: 36, fontFamily: "Kanit, sans-serif" }}>R</Avatar>
                                    </Grid>

                                    <Grid>
                                        <Grid>
                                            <Typography style={{ color: "#3C68A2", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                                Summary
                                            </Typography>
                                        </Grid>
                                        <Grid>
                                            <Typography style={{ color: "#5B81DC", fontSize: 24, fontFamily: "Kanit, sans-serif" }}>
                                                Resuscitation
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>

                                <Grid item xl={4} xs={7} container alignItems="center" direction="row" style={{}}>
                                    <Grid container alignItems="center" justify="flex-end" direction="row">
                                        <Grid container item xl={5} xs={6} direction="row" >
                                            <Grid item xl={4} xs={5}>
                                                <Typography style={{ color: "#3C68A2", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                    เริ่มต้น :
                                            </Typography>
                                            </Grid>

                                            <Grid>
                                                <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                    {moment(timeStart).format('h:mm:ss a')}
                                                </Typography>
                                            </Grid>

                                        </Grid>

                                        <Grid container item xl={5} xs={6} direction="row" >
                                            <Grid item xl={4} xs={5}>
                                                <Typography style={{ color: "#3C68A2", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                    สิ้นสุด :
                                            </Typography>
                                            </Grid>

                                            <Grid>
                                                <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                    {moment(timeEnd).format('h:mm:ss a')}
                                                </Typography>
                                            </Grid>

                                        </Grid>

                                    </Grid>

                                    <Grid container direction="row" alignItems="center" justify="center"  >
                                        <Grid item xl={3} xs={5}>
                                            <Typography style={{ color: "#3C68A2", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                เวลาที่ประเมิณ :
                                            </Typography>
                                        </Grid>

                                        <Grid item xl={3} xs={5}>
                                            <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                {processTime} นาที
                                            </Typography>
                                        </Grid>

                                    </Grid>
                                </Grid>

                            </Grid>
                        </Container>

                        <Grid container alignItems="center" style={{ backgroundColor: "#5B81DC", height: 46 }}>
                            <Container style={{ marginLeft: "4vw", marginRight: "4vw" }}>
                                <Grid container direction="row">

                                    <Grid item xl={10} xs={9} container direction="row">
                                        <Grid style={{ marginRight: 30 }}>
                                            <Typography style={{ color: "#FFFFFF", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                                ชื่อ
                                        </Typography>
                                        </Grid>

                                        <Grid>
                                            <Typography style={{ color: "#FFFFFF", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                                {data.patientInfo.value === "" ?
                                                    "-"
                                                    :
                                                    data.patientInfo.value
                                                }
                                            </Typography>
                                        </Grid>
                                    </Grid>

                                    <Grid item xl={2} xs={3} container direction="row" justify="flex-end">
                                        <Grid style={{ marginRight: 30 }}>
                                            <Typography style={{ color: "#FFFFFF", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                                อายุ
                                        </Typography>
                                        </Grid>

                                        <Grid >
                                            <Typography style={{ color: "#FFFFFF", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                                {data.age.value === "" ?
                                                    "- "
                                                    :
                                                    data.age.value
                                                }
                                                {data.ageUnit.value}
                                            </Typography>
                                        </Grid>
                                    </Grid>

                                </Grid>



                            </Container>
                        </Grid>

                        <Container style={{ marginTop: 8 }}>
                            <Grid container justify="center" alignItems="center" direction="row" style={{}}>

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={6}>
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                            Temp :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Tools.temp.value === "" ?
                                                "N/A"
                                                :
                                                Tools.temp.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={4}>
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                            BP :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                Tools.BP1.value === "" && Tools.BP2.value === "" ?
                                                    "N/A"
                                                    :
                                                    Tools.BP1.value + " / " + Tools.BP2.value
                                            }

                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={4}>
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                            PR :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Tools.PR.value === "" ?
                                                "N/A"
                                                :
                                                Tools.PR.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid item xl={1} xs={3} container direction="row" justify="center" alignItems="center">
                                    <Grid item xs={4}>
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 16, fontFamily: "Kanit, sans-serif" }}>
                                            RR :
                                        </Typography>
                                    </Grid>

                                    <Grid>
                                        <Typography style={{ color: "#819DE0", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Tools.RR.value === "" ?
                                                "N/A"
                                                :
                                                Tools.RR.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </Container>

                        <Container style={{ marginTop: 30 }}>
                            <Grid container direction="row" justify="center" alignItems="center" style={{ paddingLeft: "10vw", paddingRight: "10vw" }}>
                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            CPR
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.CPR.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.CPR.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Endotracheal tude
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.endotrachealTube.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.endotrachealTube.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            การไหลเวียนเลือดส่วนปลาย มากกว่า /วินาที
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.blood.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            ICD
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.ICD.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Arrhythmia
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.arrhythmia.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Fast track
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.fastTrack.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Apnea
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.apnea.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Seizure
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.seizure.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Alteration of consciousness
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.alteration.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Accessory muscle use
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Eyes.accessory.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            X-ray
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.xRay.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Suture
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.suture.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            EKG
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.EKG.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Consult
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.consult.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            IV Fluid
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.IVfluid.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Ultrasound
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.ultrasound.value === "1" ?
                                                "Yes"
                                                :
                                                "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Lab
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.lab.value === "1" ?
                                                "1"
                                                :
                                                Mouth.lab.value === "2" ?
                                                    ">1"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            หัตถการ
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.operative.value === "1" ?
                                                "1"
                                                :
                                                Mouth.operative.value === "2" ?
                                                    ">1"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Injection
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.injection.value === "1" ?
                                                "1"
                                                :
                                                Mouth.injection.value === "2" ?
                                                    ">1"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Suicidal attemps
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Mouth.suicidal.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            High mechanism of injury + ความเสี่ยงอื่นๆ
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                // Eyes.blood.value === "" ?
                                                //     "N/A"
                                                //     :
                                                Mouth.highMechanism.value === true ?
                                                    "Yes"
                                                    :
                                                    "N/A"
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            GSC Score
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {
                                                Mouth.TotalGSC.value === 0 ?
                                                    "N/A"
                                                    :
                                                    Mouth.TotalGSC.value
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container justify="flex-start" item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            Pain scorer(1-10)
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end" >
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.Pain.value}
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container direction="row" alignItems="center" style={{ borderBottom: "solid", borderBottomColor: "#5B81DC", }}>
                                    <Grid container item xs={11} style={{ paddingBottom: 5, paddingTop: 5 }}>
                                        <Typography style={{ color: "#5B81DC", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            SpO2(0-100%)
                                        </Typography>
                                    </Grid>

                                    <Grid item xs={1} container justify="flex-end">
                                        <Typography style={{ color: "#3C68A2", fontWeight: "bold", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                            {Mouth.SpO2.value}
                                        </Typography>
                                    </Grid>

                                </Grid>

                                <Grid container alignItems="center" justify="flex-end" direction="row" style={{ marginTop: 30.5 }}>
                                    <Grid item xl={3} xs={7}>
                                        <Typography style={{ color: "#819DE0", fontSize: 18, fontFamily: "Kanit, sans-serif" }}>
                                            จำนวนกิจกรรมทั้งหมด
                                        </Typography>
                                    </Grid>

                                    <Grid >
                                        <TextField size="small" disabled variant="outlined" style={{ width: 100, backgroundColor: "#819DE05C" }}
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <Typography style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "Kanit, sans-serif" }}>
                                                            10
                                                         </Typography>
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Container>
                    </Paper>

                    <Fab className={classes.absolute} onClick={_scrollToTop}>
                        <ArrowUpwardIcon style={{ width: 36, height: 36, color: "#FFFFFF" }} />
                    </Fab>

                </Container>


            </Grid>

        </Grid>

    );
}

export default Resulte;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    absolute: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'absolute',
        bottom: -468,
        left: "min(max(83vw),85vw)"
    },
}));
