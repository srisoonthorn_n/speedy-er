import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Container, TextField, FormControl, Select, MenuItem, } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { InfomationAction } from '../../Redux/reducer/InfoReducer';



function General_Info() {

    const classes = useStyles();
    const data = useSelector((state) => state.info.infomation);

    const [patientInfo, setpatientInfo] = useState("");
    const [age, setAge] = useState("");
    const [symptom, setSymptom] = useState("");
    const dispatch = useDispatch();



    const OnInput = (value, key) => {
        dispatch(InfomationAction(({ ...data, [key]: { ...data[key], value } })));
    };

    const onAgeInput = (event ,key) => {
        if(event >= 999) {
            const value = 999
            dispatch(InfomationAction(({ ...data, [key]: { ...data[key], value } })));
        } else {
            const value = event
            dispatch(InfomationAction(({ ...data, [key]: { ...data[key], value } })));
        }
    }

    return (
        <Grid className={classes.gridBody}>
            <Grid container >
                <Container>
                    <Paper className={classes.paperStyle}>
                        <Grid className={classes.gridBodyPaper}>
                            <Grid className={classes.girdHeaderText}>
                                <Typography className={classes.TextHeader}>
                                    General Information
                                </Typography>
                            </Grid>
                            <Grid
                                container
                                direction="row"
                                justify="center"
                                spacing={1}
                            >
                                <Grid item xs={12} md={5} xl={5}>
                                    <Grid className={classes.girdBodyTextField}>
                                        <TextField
                                            value={data.patientInfo.value}
                                            defaultValue={patientInfo}
                                            onChange={(e) => {
                                                setpatientInfo(e.target.value);
                                                OnInput(e.target.value, 'patientInfo');
                                            }}
                                            id="outlined-basic" label="( ระบุ ) ชื่อ-นามสกุล , รหัสประจำตัว" variant="outlined"
                                            className={classes.textFieldStyle} />
                                    </Grid>

                                    <Grid >
                                        <TextField
                                            value={data.symptom.value}
                                            defaultValue={symptom}
                                            onChange={(e) => {
                                                setSymptom(e.target.value);
                                                OnInput(e.target.value, 'symptom');
                                            }}
                                            id="outlined-basic" label="อาการ" variant="outlined"
                                            className={classes.textFieldStyle} />
                                    </Grid>
                                </Grid>

                                <Grid
                                    item xs={7} sm={6} md={4} xl={4}
                                    justify="center"
                                    container
                                    direction="row"

                                >
                                    <Grid item xs={5} xl={6} className={classes.gridTextFieldAge}>
                                        <TextField
                                            value={data.age.value}
                                            type="number"
                                            defaultValue={age}
                                            onChange={(e) => {
                                                setAge(e.target.value);
                                                onAgeInput(e.target.value, 'age');
                                            }}
                                            id="outlined-basic" label="อายุ" variant="outlined"
                                            className={classes.textFieldAgeStyle} />
                                    </Grid>
                                    <Grid item xs={5} xl={5} >
                                        <FormControl variant="outlined" className={classes.formControl}>
                                            <Select
                                                className={classes.selectStyle}
                                                defaultValue={data.ageUnit.value}
                                                value={data.ageUnit.value}
                                                onChange={(e) => OnInput(e.target.value, "ageUnit")}>
                                                <MenuItem value={"ปี"}>ปี</MenuItem>
                                                <MenuItem value={"เดือน"}>เดือน</MenuItem>

                                            </Select>
                                        </FormControl>
                                    </Grid>

                                </Grid>

                            </Grid>

                        </Grid>
                    </Paper>

                </Container>

            </Grid>




        </Grid>

    );
}

export default General_Info;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        width: "100%",
        textAlign: "start"
    },
    gridBody: {
        backgroundColor: "#FAFAFA",
        width: "100%"
    },
    paperStyle: {
        paddingBottom: 83
    },
    gridBodyPaper: {
        paddingTop: 37,
        textAlign: "center"
    },
    girdHeaderText: {
        marginBottom: 57
    },
    TextHeader: {
        color: "#5B81DC",
        fontSize: 20,
        fontWeight: "bold",
        fontFamily: "Kanit, sans-serif"
    },
    girdBodyTextField: {
        marginBottom: 7.5
    },
    textFieldStyle: {
        minWidth: "19.5vw",
        fontFamily: "Kanit, sans-serif"
    },
    textFieldAgeStyle : {
        maxWidth: "25vw", minWidth: "6vw"
    },
    gridTextFieldAge :{
        marginRight: 6 
    },
    selectStyle : {
        color: "#919191"
    }
}));
