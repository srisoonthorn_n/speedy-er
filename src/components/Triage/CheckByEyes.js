import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import { Typography, Paper, Container, Switch, } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { checkByEyesAction } from '../../Redux/reducer/InfoReducer';




function CheckByEyes() {

    const classes = useStyles();

    const [checked, setChecked] = React.useState([]);

    const data = useSelector((state) => state.info.checkByEyes)

    const dispatch = useDispatch();

    const handleSwitch = (event, status) => () => {
        const currentIndex = checked.indexOf(event);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            const value = !status
            newChecked.push(event);

            dispatch(checkByEyesAction(({ ...data, [event]: { ...data[event], value } })));
        } else {
            const value = !status
            newChecked.splice(currentIndex, 1);
            dispatch(checkByEyesAction(({ ...data, [event]: { ...data[event], value } })));
        }

        setChecked(newChecked);
    }

    return (
        <Grid className={classes.gridBody}>
            <Grid container>
                <Container>
                    <Paper className={classes.paperStyle}>
                        <Grid className={classes.paperGridBody}>
                            <Grid className={classes.gridHeaderText}>
                                <Typography className={classes.headerText}>
                                    Check symbol by eyes
                                </Typography>
                            </Grid>

                            <Container className={classes.containerInBody}>
                                <Grid
                                    container
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                >
                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                CPR
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.CPR.value}
                                                onChange={handleSwitch('CPR', data.CPR.value)}
                                                checked={data.CPR.value !== false}

                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Endotracheal tube
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.endotrachealTube.value}
                                                onChange={handleSwitch('endotrachealTube', data.endotrachealTube.value)}
                                                checked={data.endotrachealTube.value !== false}

                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                การไหลเวียนเลือดส่วนปลาย มากกว่า /วินาที
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.blood.value}
                                                onChange={handleSwitch('blood', data.blood.value)}
                                                checked={data.blood.value !== false}

                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                ICD
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.ICD.value}
                                                onChange={handleSwitch('ICD', data.ICD.value)}
                                                checked={data.ICD.value !== false}

                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Arrhythmia
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.arrhythmia.value}
                                                onChange={handleSwitch('arrhythmia', data.arrhythmia.value)}
                                                checked={data.arrhythmia.value !== false}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Fast track
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.fastTrack.value}
                                                onChange={handleSwitch('fastTrack', data.fastTrack.value)}
                                                checked={data.fastTrack.value !== false}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Apnea
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.apnea.value}
                                                onChange={handleSwitch('apnea', data.apnea.value)}
                                                checked={data.apnea.value !== false}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Seizure
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.seizure.value}
                                                onChange={handleSwitch('seizure', data.seizure.value)}
                                                checked={data.seizure.value !== false}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Alteration of consciousness
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.alteration.value}
                                                onChange={handleSwitch('alteration', data.alteration.value)}
                                                checked={data.alteration.value !== false}
                                            />
                                        </Grid>
                                    </Grid>

                                    <Grid container alignItems="center" direction="row" item xs={12} xl={6}>
                                        <Grid item xs={10} xl={7}>
                                            <Typography className={classes.textItemStyle}>
                                                Accessory muscle use
                                            </Typography>
                                        </Grid>

                                        <Grid item xs={1} xl={2}>
                                            <Switch
                                                value={data.accessory.value}
                                                onChange={handleSwitch('accessory', data.accessory.value)}
                                                checked={data.accessory.value !== false}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Container>

                        </Grid>
                    </Paper>
                </Container>

            </Grid>




        </Grid>

    );
}

export default CheckByEyes;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    gridBody: {
        backgroundColor: "#FAFAFA",
        width: "100%"
    },
    paperStyle: {
        paddingBottom: 83
    },
    paperGridBody: {
        paddingTop: 37,
        textAlign: "center"
    },
    gridHeaderText: {
        marginBottom: 57
    },
    headerText: {
        color: "#5B81DC",
        fontSize: 20,
        fontWeight: "bold",
        fontFamily: "Kanit, sans-serif"
    },
    containerInBody: {
        paddingLeft: "5vw",
        paddingRight: "5vw"
    },
    textItemStyle : {
        textAlign: "start", 
        color: "#5B81DC", 
        fontSize: "min(max(2vmax, 14px), 16px)", 
        fontFamily: "Kanit, sans-serif"
    }
}));
