import { Fab, Grid, Hidden, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import { useWindowScroll } from 'react-use';

const ScrollToTop = () => {
    const { y: pageYoffset } = useWindowScroll();
    const [visible, setVisible] = useState(false);
    const classes = useStyles();

    useEffect(() => {
        if (pageYoffset > 400) {
            setVisible(true)

        } else {
            setVisible(false)
        }
    }, [pageYoffset])

    const _scrollToTop = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    if (!visible) {
        return false;
    }

    return (
        <Grid>

            <Hidden only={["xs", "sm", "md", "xl"]}>
                <Fab className={classes.hiddenLg} onClick={_scrollToTop}>
                    <ArrowUpwardIcon className={classes.iconStyle} />
                </Fab>
            </Hidden>

            <Hidden only={["xl", "lg"]} >
                <Fab className={classes.hiddenMd} onClick={_scrollToTop}>
                    <ArrowUpwardIcon className={classes.iconStyle} />
                </Fab>
            </Hidden>

            <Hidden only={["xs", "sm", "md", "lg"]} >
                <Fab className={classes.hiddenXl} onClick={_scrollToTop}>
                    <ArrowUpwardIcon className={classes.iconStyle} />
                </Fab>
            </Hidden>
        </Grid>

    )

}

export default ScrollToTop;


const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    absolute: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: 112,
        right: 0,
        zIndex: 99
    },
    iconStyle: {
        width: 36,
        height: 36,
        color: "#FFFFFF"
    },
    hiddenLg: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: 112,
        zIndex: 99,
        right: 50,
    },
    hiddenMd: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: 110,
        zIndex: 99,
        right: 0,
    },
    hiddenXl: {
        backgroundColor: "#5B81DC",
        width: 64,
        height: 64,
        position: 'fixed',
        bottom: "max(min(6vw),6vw)",
        zIndex: 99,
        left: "min(max(86vw),86vw)",
    }
}));
