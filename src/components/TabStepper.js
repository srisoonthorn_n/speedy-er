import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import clsx from 'clsx';
import { Stepper, StepLabel, Step, withStyles, StepConnector, Typography, Button, Container, Modal, Icon, Hidden } from '@material-ui/core';
import General_Info from './Triage/General_Info';
import CheckByEyes from './Triage/CheckByEyes';
import CheckByTools from './Triage/CheckByTools';
import Login from '../screen/Login';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CheckByMouth from './Triage/CheckByMouth';
import { onEnd, onStart, onValueResulte } from '../Redux/reducer/InfoReducer';
import { useDispatch, useSelector } from 'react-redux';
import ResulteMap from './Triage/ResulteMap';

function getSteps() {
    return ['Patient Information', 'Check symbol by eyes', 'Check symbol by mouth', 'Check symbol by tools', 'Result'];
}


function getStepContent(step) {
    switch (step) {
        case 0:
            return 'Patient Information';
        case 1:
            return 'Basic observation';
        case 2:
            return 'Check up';
        case 3:
            return 'Vital Sign';
        case 4:
            return 'Result';
        default:
            return 'Unknown step';
    }
}

function getStepPage(step) {
    switch (step) {
        case 0:
            return <General_Info />;
        case 1:
            return <CheckByEyes />;
        case 2:
            return <CheckByMouth />;
        case 3:
            return <CheckByTools />;
        case 4:
            // return <Resulte />
            return <ResulteMap />
        default:
            return <Login />
    }
}

const ColorlibConnector = withStyles({
    alternativeLabel: {
        top: 14,
    },
    active: {
        '& $line': {
            marginRight: 10,
            marginLeft: 10,
            backgroundColor:
                '#5B81DC',
        },
    },
    completed: {
        '& $line': {
            marginRight: 10,
            marginLeft: 10,
            backgroundColor:
                '#5B81DC',
        },
    },
    line: {
        height: 1,
        border: 0,
        backgroundColor: '#C8C8C8',
        borderRadius: 1,
        marginRight: 10,
        marginLeft: 10,
    },
})(StepConnector);

const ColorlibConnectorSm = withStyles({
    alternativeLabel: {
        top: 14,
    },
    active: {
        '& $line': {

            backgroundColor:
                '#5B81DC',
        },
    },
    completed: {
        '& $line': {
            backgroundColor:
                '#5B81DC',
        },
    },
    line: {
        height: 1,
        border: 0,
        backgroundColor: '#C8C8C8',
        borderRadius: 1,
        // marginRight: 10,
        // marginLeft: 10,
    },
})(StepConnector);


const useColorlibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#757679',
        zIndex: 1,
        color: '#fff',
        width: 30,
        height: 30,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    active: {
        backgroundColor:
            '#5B81DC',
    },
    completed: {
        backgroundColor:
            '#5B81DC',
    },
    imageIconStepper: {
        width: 40,
        height: 40
    },
    imageIconSteperInfo: {
        width: 30,
        height: 30
    },
    reslute: {
        fontSize: 20
    }
});


function ColorlibStepIcon(props) {
    const classes = useColorlibStepIconStyles();
    const { active, completed } = props;

    const icons = {
        1: <img src={require('../assets/images/icons_info.png')} className={classes.imageIconSteperInfo} />,
        2: completed || active ?
            <img src={require('../assets/images/observe-active.png')} className={classes.imageIconStepper} />
            :
            <img src={require('../assets/images/observe.png')} className={classes.imageIconStepper} />,
        3: completed || active ?
            <img src={require('../assets/images/checkup-active.png')} className={classes.imageIconStepper} />
            :
            <img src={require('../assets/images/checkup.png')} className={classes.imageIconStepper} />,
        4: completed || active ?
            <img src={require('../assets/images/vitalSign-active.png')} className={classes.imageIconStepper} />
            :
            <img src={require('../assets/images/vitalSign.png')} className={classes.imageIconStepper} />,
        5: <Typography classes={classes.reslute} >R</Typography>,
    };

    return (
        <div
            className={clsx(classes.root, {
                [classes.active]: active,
                [classes.completed]: completed,
            })}
        >
            {icons[String(props.icon)]}
        </div>
    );
}

function TabStepper() {

    const classes = useStyles();
    const steps = getSteps();
    const [activeStep, setActiveStep] = React.useState(0);
    const valueResulte = useSelector((state) => state.info.valueOfResulte);


    // const [completed, setCompleted] = React.useState({});
    // const [age, setAge] = useState("ปี")

    const dispatch = useDispatch();

    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);


    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        window.location.href = "/triage"
    };

    const body = (
        <div style={modalStyle} className={classes.paper}>
            <Grid className={classes.gridIcon}>
                <Icon><CheckCircleIcon className={classes.iconCheck} /></Icon>
                <Typography className={classes.textSuccssful}>SUCESSFUL</Typography>
                <Typography className={classes.textDes}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore magna aliqua.
                </Typography>

            </Grid>
            <Button variant="contained" className={classes.buttonOk} onClick={handleClose}>OK</Button>
        </div>
    );



    const handleStep = (step) => () => {
        setActiveStep(step);
    };


    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };


    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleStart = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        dispatch(onStart(new Date()));
    }

    const handleFinish = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        dispatch(onValueResulte("Resulte"));
        dispatch(onEnd(new Date()));
    }

    return (
        <Grid className={classes.gridTabScreen}>
            <Grid container justify="center">
                <Hidden xsDown>
                    <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />} className={classes.stepperStyleXs}>
                        {steps.map((label, index) => (
                            <Step key={label} >
                                {
                                    valueResulte !== "Resulte" && index != 4 ?
                                        <StepLabel StepIconComponent={ColorlibStepIcon} onClick={handleStep(index)} />
                                        :
                                        valueResulte === "" && index == 4 ?
                                            <StepLabel StepIconComponent={ColorlibStepIcon} />
                                            :
                                            <StepLabel StepIconComponent={ColorlibStepIcon} onClick={handleStep(index)} />

                                }

                            </Step>
                        ))}
                    </Stepper>
                </Hidden>

                <Hidden smUp>
                    <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnectorSm />} className={classes.stepperStyleSm}>
                        {steps.map((label, index) => (
                            <Step key={label} >
                                {
                                    valueResulte !== "Resulte" && index != 4 ?
                                        <StepLabel StepIconComponent={ColorlibStepIcon} onClick={handleStep(index)} />
                                        :
                                        valueResulte === "" && index == 4 ?
                                            <StepLabel StepIconComponent={ColorlibStepIcon} />
                                            :
                                            <StepLabel StepIconComponent={ColorlibStepIcon} onClick={handleStep(index)} />
                                }
                            </Step>
                        ))}
                    </Stepper>
                </Hidden>

            </Grid>
            <Grid container justify="center" className={classes.girdShowStep}>
                <Typography className={classes.textShowStep}>
                    {getStepContent(activeStep)}
                </Typography>
            </Grid>

            <Grid>
                {getStepPage(activeStep)}
            </Grid>



            <Container>
                {activeStep === steps.length - 1 ?
                    <Grid container justify="center" >
                        <Button variant="contained" onClick={handleOpen} className={classes.buttonDone}>
                            เสร็จสิ้นและบันทึกผล
                        </Button>

                    </Grid>
                    :
                    <Grid container direction="row">
                        <Grid container className={classes.girdbuttonBackStep} item xs={7} justify="flex-start" >
                            {
                                activeStep === 0 ?
                                    <div></div>
                                    :
                                    <Button onClick={handleBack} variant="outlined" className={classes.buttonBackStep}>
                                        Back step
                                        </Button>
                            }

                        </Grid>

                        <Grid container className={classes.gridFinishAndNext} item xs={5} justify="flex-end">
                            {activeStep === steps.length - 2 ?
                                <Button
                                    variant="contained"
                                    onClick={handleFinish}
                                    className={classes.buttonFinish}>
                                    Finish
                                </Button>
                                :
                                activeStep === 0 ?
                                    <Button variant="contained" onClick={handleStart} className={classes.buttonNextStep}>
                                        Next Step
                                    </Button>
                                    :

                                    <Button variant="contained" onClick={handleNext} className={classes.buttonNextStep}>
                                        Next Step
                                </Button>
                            }
                        </Grid>
                    </Grid>
                }
            </Container>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </Grid>

    );
}

export default TabStepper;

const useStyles = makeStyles((theme) => ({
    button: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    formControl: {
        minWidth: "9vw",
        textAlign: "start"
    },
    paper: {
        position: 'absolute',
        width: 249,
        backgroundColor: theme.palette.background.paper,

        boxShadow: theme.shadows[5],
        padding: 30,
        textAlign: "center"
    },
    gridIcon: {
        marginBottom: 47
    },
    iconCheck: {
        color: "#5B81DC",
        width: 90,
        height: 90
    },
    textSuccssful: {
        color: "#5B81DC",
        fontWeight: "bold",
        fontSize: 16,
        fontFamily: "Kanit, sans-serif",
        marginTop: 19
    },
    textDes: {
        color: "#5B81DC5E",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif",
        marginTop: 19
    },
    buttonOk: {
        width: "70%",
        backgroundColor: "#5B81DC",
        color: "#FFFFFF",
        fontSize: 14,
        fontFamily: "Kanit, sans-serif"
    },
    gridTabScreen: {
        marginTop: 40,
        marginBottom: 56
    },
    stepperStyleXs: {
        backgroundColor: "#FAFAFA",
        maxWidth: "40vw",
        minWidth: "37vmax",
        marginBottom: 14
    },
    stepperStyleSm: {
        backgroundColor: "#FAFAFA",
        maxWidth: "40vw",
        minWidth: "49vmax",
        marginBottom: 14
    },
    girdShowStep: {
        width: "100%",
        marginBottom: "5vw"
    },
    textShowStep: {
        color: "#819DE0",
        fontSize: 14,
        fontStyle: "italic",
        fontFamily: "Kanit, sans-serif"
    },
    buttonDone: {
        backgroundColor: "#5B81DC",
        width: 356,
        height: 36,
        color: "#FFFFFF",
        fontFamily: "Kanit, sans-serif",
    },
    girdbuttonBackStep: {
        marginTop: 20.5
    },
    buttonBackStep: {
        backgroundColor: "#FFFFFF",
        width: 130,
        height: 36,
        borderColor: "#5B81DC",
        border: "solid",
        color: "#5B81DC",
        fontFamily: "Kanit, sans-serif"
    },
    gridFinishAndNext: {
        marginTop: 20.5
    },
    buttonFinish: {
        backgroundColor: "#5B81DC",
        width: 130,
        height: 36,
        color: "#FFFFFF",
        fontFamily: "Kanit, sans-serif",
    },
    buttonNextStep: {
        backgroundColor: "#5B81DC",
        width: 130,
        height: 36,
        color: "#FFFFFF",
        fontFamily: "Kanit, sans-serif",
    }


}));




function getModalStyle() {
    const top = 50
    const left = 50

    return {
        top: `${40}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}
