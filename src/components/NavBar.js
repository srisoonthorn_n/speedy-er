import React, { useEffect, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Grid, Menu, MenuItem, Tab, Tabs, Button, withStyles, ListItemIcon, ListItemText, ListItem, Hidden, IconButton, Drawer, Typography } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../Redux/reducer/authUser';
import { onEnd } from '../Redux/reducer/InfoReducer';
import clsx from 'clsx';

import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';


const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
            },
        },
    },
}))(MenuItem);


function NavBar(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [value, setValue] = React.useState(0);
    const userName = useSelector((state) => state.auth)
    const dispatch = useDispatch();
    const [isDrawerOpen, setIsDrawerOpen] = useState(false)

    // useEffect(() => {
    //     checkToken()
    // }, [])

    // const checkToken = () => {
    //     if (userName.token == "") {
    //         return window.location.href = "/"
    //     }
    // }

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            onClick={() => setIsDrawerOpen(false)}
            onKeyDown={() => setIsDrawerOpen(false)}
        >
            <Grid container alignItems="center" justify="center" className={classes.gridBodyList}>
                <Grid item xs={6}>
                    <Typography component="h1" className={classes.textHeaderList}>
                        Speedy Er
                    </Typography>
                </Grid>

                <Grid item xs={6} container alignItems="center" justify="flex-end">
                    <Button className={classes.buttonStyleList} onClick={setIsDrawerOpen}>
                        <ArrowBackIosIcon className={classes.iconStyleList} />
                    </Button>

                </Grid>
            </Grid>

            <Grid className={classes.gridBodyListItem}>
                <ListItem button key={"Triage"} onClick={() => window.location.href = "/triage"}>
                    <ListItemText><p className={classes.textStyleListItem}>Triage</p></ListItemText>
                </ListItem>

                <ListItem button key={"Fasttrack"} >
                    <ListItemText><p className={classes.textStyleListItem}>Fasttrack</p></ListItemText>
                </ListItem>

                <ListItem button key={"Moph Ed. reference"} >
                    <ListItemText><p className={classes.textStyleListItem}>Moph Ed. reference</p></ListItemText>
                </ListItem>
            </Grid>

        </div>
    )


    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
    };

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const onLogout = () => {
        dispatch(logout());
        dispatch(onEnd());
        window.location.href = "/"
    }
    return (
        <AppBar position="static" className={classes.appbarStyle}>
            <Toolbar className={classes.toolbarStyle}>

                <Hidden smUp>
                    <Grid item xs={5} className={classes.gridBodySm}>
                        <IconButton
                            edge="start"
                            className={classes.menuButton}
                            onClick={() => setIsDrawerOpen(true)}
                        >
                            <MenuIcon />
                        </IconButton>
                    </Grid>
                </Hidden>

                <Grid item xl={1} sm={1} xs={5} container alignItems="center" className={classes.gridImageIcon} onClick={() => window.location.href = "/triage"}>
                    <img src={require('../assets/images/logo.jpg')} className={classes.imageLogo} />
                </Grid>

                <Hidden xsDown>
                    <Grid
                        item xl={11} sm={11} xs={10}
                        className={classes.gridTabsXs}
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >

                        <Tabs
                            id="TabsBar"
                            value={value}
                            onChange={handleChangeTab}
                            indicatorColor="primary"
                            textColor="primary"
                            centered
                        >
                            <Tab label="Triage" />
                            <Tab label="Fasttrack" />
                            <Tab label="Moph Ed. reference" />
                        </Tabs>



                    </Grid>
                </Hidden>



                <Grid item xl={1} xs={2} container direction="row" alignItems="center" justify="flex-end" className={classes.gridBodyProfile} >
                    <Grid>
                        <Button
                            aria-controls="customized-menu"
                            aria-haspopup="true"
                            onClick={handleClick}
                        >
                            <Avatar alt={userName.username} src="/broken-image.jpg"  className={classes.iconProfile} />
                            <ArrowDropDownIcon />
                        </Button>
                        <StyledMenu
                            id="customized-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <StyledMenuItem onClick={() => onLogout()}>
                                <ListItemIcon className={classes.listIconProfile}>
                                    <ExitToAppIcon className={classes.iconListStyle} fontSize="small" />
                                </ListItemIcon>
                                <ListItemText className={classes.iconListText} primary="Logout" />
                            </StyledMenuItem>
                        </StyledMenu>
                    </Grid>



                </Grid>

            </Toolbar>

            <Drawer anchor={'left'} open={isDrawerOpen} onClose={() => setIsDrawerOpen(false)}>
                <Grid>
                    {list('left')}
                </Grid>

            </Drawer>
        </AppBar >
    );
}

export default NavBar;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
            color: "#819DE0" 
        },
        title: {
            flexGrow: 1,
            fontSize: 24,
            textShadow: '0 0 20px #60D7FF',

        },
        manuBar: {
            fontSize: 16,
            fontFamily: "Kanit, sans-serif",
            color: "#D4D4D4"
        },
        menuBarAction: {
            color: "#819DE0",
            fontSize: 16,
            fontFamily: "Kanit, sans-serif"
        },
        appbarStyle: {
            backgroundColor: "#FFFFFF",
            height: 72,
        },
        imageLogo: {
            width: 57,
            height: 56,
        },
        gridBodyList: {
            padding: 20,
            borderWidth: 1,
            borderBottomStyle: "groove"
        },
        textHeaderList: {
            color: "#5B81DC",
            fontWeight: "bold",
            fontSize: 18,
            fontFamily: "Kanit, sans-serif"
        },
        buttonStyleList: {
            height: 50
        },
        iconStyleList: {
            color: "#5B81DC",
            width: 16, height: 16
        },
        gridBodyListItem: {
            paddingBottom: 26,
            borderWidth: 1,
            borderBottomStyle: "groove"
        },
        textStyleListItem: {
            color: '#000000',
            fontSize: 14,
            margin: 0,
            padding: 0,
            paddingRight: "10vw",
            paddingTop: 4
        },
        toolbarStyle : {
            marginTop: 5 
        },
        gridBodySm : {
            display: 'flex',
        },
        gridImageIcon : {
            flexGrow: 1
        },
        gridTabsXs :{ 
            paddingBottom: 5
        },
        gridBodyProfile : {
            flexGrow: 1
        },
        iconProfile : {
            width: 32, 
            height: 32, 
            fontWeight: "bold", 
            fontSize: 16, 
            fontFamily: "Kanit, sans-serif" 
        },
        listIconProfile : {
            minWidth: 33
        },
        iconListStyle : {
            color: "#5B81DC"
        },
        iconListText : {
            color: "#5B81DC", 
            fontWeight: 12, 
            fontFamily: "Kanit, sans-serif",
        }
 
    }),
);


const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));



{/* <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={opened}
                        onClose={handleClosed}
                    >
                        <MenuItem onClick={handleClose}>Logout</MenuItem>
                    </Menu> */}