import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import NavBar from '../components/NavBar';
import { Grid } from '@material-ui/core';
import TabStepper from '../components/TabStepper';





function Triage(props) {

    const classes = useStyles();
    return (
        <Grid container justify="center"  className={classes.root} >
            <NavBar props={props}/>
            <TabStepper />
        </Grid>

    );
}

export default Triage;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'table',
        backgroundColor: "#FAFAFA"
    },
}));
