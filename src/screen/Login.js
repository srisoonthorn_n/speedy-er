import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Hidden, Input } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../Redux/reducer/authUser';



function Login(props) {

    const classes = useStyles();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const onLogin = () => {
        const token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        // console.log(alert(username + " " + password + " " + token))
        dispatch(loginSuccess(username, token))
        props.history.push('/triage')
    }

    return (
        <Grid container component="main" className={classes.root}>

            <Hidden xsDown>
                <Grid item xs={false} sm={6} md={7} xl={8} container alignItems="center" justify="center" className={classes.image}>
                    <img src={require('../assets/images/logoInWebErSpeedy_loginV2.png')} className={classes.imageLogoXs} />
                </Grid>
            </Hidden>

            <Grid item xs={12} sm={6} md={5} xl={4} className={classes.gridLogin} container alignItems="center" justify="center" >
                <Container>
                    <Grid >
                        <Hidden smUp>
                            <Grid className={classes.gridImageLogoSm}>
                                <img src={require('../assets/images/logo.jpg')} className={classes.imageLogoSm} />
                            </Grid>
                        </Hidden>

                        <Typography component="h1" className={classes.TextSignIn}>
                            Sign in
                        </Typography>
                        <form className={classes.form} noValidate>
                            <Grid className={classes.girdInput}>
                                <Grid>
                                    <p className={classes.styleP}>Username</p>
                                </Grid>

                                <Grid>
                                    <Input
                                        classes={classes.styleInput}
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        value={username}
                                        id="username"
                                        onChange={(e) => setUsername(e.target.value)}
                                        placeholder="Enter your username"
                                        autoComplete="email"
                                        autoFocus
                                    />
                                </Grid>
                            </Grid>

                            <Grid >
                                <Grid>
                                    <p className={classes.styleP}>Password</p>
                                </Grid>
                                <Grid >
                                    <Input
                                        classes={classes.styleInput}
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        onChange={(e) => setPassword(e.target.value)}
                                        placeholder="Enter your password"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                    />
                                </Grid>
                            </Grid>

                            <Grid container justify="center" classes={classes.girdButton}>
                                <Button
                                    onClick={() => onLogin()}
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    className={classes.submit}
                                >
                                    Sign In
                            </Button>
                            </Grid>

                        </form>
                    </Grid>
                </Container>

            </Grid>
        </Grid>

    );
}

export default Login;

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100vh"
    },
    image: {
        textAlign: "center",
        backgroundColor: "#819DE0",
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        paddingLeft: 72.5,
        paddingRight: 72.5,
        marginTop: theme.spacing(1),
    },
    submit: {
        width: 280,
        backgroundColor: "#819DE0",
        color: "#FFFFFF",
        margin: theme.spacing(3, 0, 2),
        fontSize: 16,
        fontFamily: "Kanit, sans-serif",
    },
    imageLogoXs: {
        width: "25vw",
        height: "27vw",
    },
    gridLogin: {
        backgroundColor: "#FFFFFF"
    },
    gridImageLogoSm: {
        textAlign: "center"
    },
    imageLogoSm: {
        width: "25vw",
        height: "25vw",
    },
    TextSignIn: {
        textAlign: "center",
        color: "#5B81DC",
        fontWeight: "bold",
        fontSize: 36,
        fontFamily: "Kanit, sans-serif"
    },
    girdInput: {
        marginBottom: 16
    },
    styleP: {
        color: "#819DE0",
        fontSize: 18
    },
    styleInput: {
        color: "#919191",
        fontFamily: "Kanit, sans-serif",
        fontSize: 16
    },
    girdButton : {
        marginTop: 48
    }
}));
