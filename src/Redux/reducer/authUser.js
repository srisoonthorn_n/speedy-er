const LOGIN = "LOGIN";
const LOGOUT = "LOGOUT";

export const loginSuccess = (username, token) => ({
  type: LOGIN,
  username: username,
  payload: token
});

export const logout = () => ({
  type: LOGOUT,
});


const INITIAL_STATE = {
  isLogin: false,
  username: "",
  token: "",
};

export const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isLogin: true,
        token: action.payload,
        username: action.username
      };
    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};
