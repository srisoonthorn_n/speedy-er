import { mockInfo } from "../../mock/mockInfo";

const INFO = "INFO";
const Check_By_Mouth = "Check_By_Mouth";
const Check_By_Eyes = "Check_By_Eyes";
const Check_By_Tools = "Check_By_Tools";
const ON_START = "ON_START";
const ON_END = "ON_END";
const ON_TOTAL_ACTIVITY = "ON_TOTAL_ACTIVITY";
const ON_VALUE_RESULTE = "ON_VALUE_RESULTE";


export const InfomationAction = (data) => ({
    type: INFO,
    payload: data
});


export const checkByEyesAction = (data) => ({
    type: Check_By_Eyes,
    payload: data
})
export const checkByMouthAction = (data) => ({
    type: Check_By_Mouth,
    payload: data
});

export const checkByToolsAction = (data) => ({
    type: Check_By_Tools,
    payload: data
});

export const onStart = (startTime) => ({ type: ON_START, payLoad: startTime });

export const onEnd = (endTime) => ({ type: ON_END, payLoad: endTime });


export const onTotalAct = (activityTotal) => ({ type: ON_TOTAL_ACTIVITY, payLoad: activityTotal });

export const onValueResulte = (valueOfResulte) => ({
    type: ON_VALUE_RESULTE,
    payLoad: valueOfResulte
})



const INITIAL_STATE = {
    ...mockInfo
};

export const InfoReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case INFO:
            return {
                ...state,
                infomation: action.payload
            };
        case Check_By_Eyes:
            return {
                ...state,
                checkByEyes: action.payload
            }
        case Check_By_Mouth:
            return {
                ...state,
                checkByMouth: action.payload
            };
        case Check_By_Tools:
            return {
                ...state,
                checkByTools: action.payload
            };
        case ON_START:
            return { ...state, startTime: action.payLoad, isStart: true };
        case ON_END:
            return { ...state, endTime: action.payLoad, isStart: false };
        case ON_TOTAL_ACTIVITY:
            return {
                ...state,
                activityTotal: action.payLoad,

            }
        case ON_VALUE_RESULTE:
            return {
                ...state,
                valueOfResulte: action.payLoad,
            }
        default:
            return state;
    }
};
