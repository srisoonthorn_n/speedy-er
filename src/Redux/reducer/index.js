
import { authReducer } from "./authUser";
import { combineReducers } from "redux";
import { InfoReducer } from "./InfoReducer";



export const rootReducer = combineReducers({
    auth: authReducer,
    info: InfoReducer,
});
