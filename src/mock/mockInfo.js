export const mockInfo = {
    infomation: {
        patientInfo: { title: "PatientInfo", value: "" },
        age: { title: "Age", value: "" },
        ageUnit: { title: "AgeUnit", value: "ปี" },
        symptom: { title: "Symptom", value: "" },
    },
    checkByEyes: {
        CPR: { title: "CPR", value: false },
        endotrachealTube: { title: "Endotracheal Tube", value: false },
        blood: { title: "การไหลเวียนเลือดส่วนปลาย มากกว่า /วินาที", value: false },
        ICD: { title: "ICD", value: false },
        arrhythmia: { title: "Arrhythmia", value: false },
        fastTrack: { title: "Fast Track", value: false },
        apnea: { title: "Apnea", value: false },
        seizure: { title: "Seizure", value: false },
        alteration: { title: "Alteration of consciousness", value: false },
        accessory: { title: "Accessory muscle use", value: false },
    },
    checkByMouth: {
        lab: { title: "LAB", value: "0" },
        operative: { title: "หัตถการ", value: "0" },
        injection: { title: "Injection", value: "0" },
        xRay: { title: "X-Ray", value: false },
        suture: { title: "Suture", value: false },
        EKG: { title: "EKG", value: false },
        consult: { title: "Consult", value: false },
        IVfluid: { title: "IV Fluid", value: false },
        ultrasound: { title: "Ultrasound", value: false },
        suicidal: { title: "Suicidal attemps", value: false },
        highMechanism : { title: "High mechanism of injury + ความเสี่ยงอื่นๆ", value: false },
        E: { title: "E", value: 0 },
        V: { title: "V", value: 0 },
        M: { title: "M", value: 0 },
        TotalGSC: { title: "GSC Score", value: 0 },
        Pain: { title: "Pain score(1-10)", value: 1 },
        SpO2: { title: "SpO2 (0-100%)", value: 0 },
    },
    checkByTools: {
        temp: { title: "Temp", value: "" },
        RR: { title: "RR", value: "" },
        PR: { title: "PR", value: "" },
        BP1: { title: "BP1", value: "" },
        BP2: { title: "BP2", value: "" },
    },
    startTime: null,
    endTime: null,
    isStart: false,
    activityTotal : 0 ,
    valueOfResulte : "",

};

