import React from 'react';
import './App.css';
import Login from './screen/Login';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Triage from './screen/Triage';

function App() {
  return (
    <BrowserRouter>
      <Switch location={window.location}>
      <Route path="/" component={Login} exact />
      <Route path="/triage" component={Triage} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
